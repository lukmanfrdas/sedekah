<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ route('home') }}" aria-expanded="false">
                        <i class="icon-Car-Wheel"></i>
                        <span class="hide-menu">Dashboard </span>
                    </a>
                </li>

                @can('user.view', 'permission.view', 'role.view')
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
                            aria-expanded="false">
                            <i class="mdi mdi-account-multiple"></i>
                            <span class="hide-menu">User </span>
                        </a>
                        <ul aria-expanded="false" class="collapse first-level">
                            @can('user.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('user.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-email"></i>
                                        <span class="hide-menu"> User List </span>
                                    </a>
                                </li>
                            @endcan

                            @can('permission.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('permission.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-adjust"></i><span class="hide-menu">User Permission</span>
                                    </a>
                                </li>
                            @endcan

                            @can('role.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('role.index') }}" class="sidebar-link"><i
                                            class="mdi mdi-adjust"></i><span class="hide-menu">User Role</span>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('balance.view')
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
                            aria-expanded="false"><i class="mdi mdi-cash-multiple"></i><span
                                class="hide-menu">Keuangan</span></a>

                        <ul aria-expanded="false" class="collapse first-level">
                            @can('balance.view')
                                <li class="sidebar-item">
                                    <a href="{{ route('balance.index') }}" class="sidebar-link">
                                        <i class="mdi mdi-cash-multiple"></i>
                                        <span class="hide-menu"> Riwayat Saldo</span>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('category-donation.view')
                <li class="sidebar-item">
                    <a href="{{ route('category-donation.index') }}" class="sidebar-link">
                        <i class="fas fa-bars"></i>
                        <span class="hide-menu"> Kategori Donasi </span>
                    </a>
                </li>
                @endcan

                @can('collector.view')
                <li class="sidebar-item">
                    <a href="{{ route('collector.index') }}" class="sidebar-link">
                        <i class="fas fa-user-circle"></i>
                        <span class="hide-menu"> Collector </span>
                    </a>
                </li>
                @endcan

                @can('donor.view')
                <li class="sidebar-item">
                    <a href="{{ route('donor.index') }}" class="sidebar-link">
                        <i class="fas fa-users"></i>
                        <span class="hide-menu"> Donor </span>
                    </a>
                </li>
                @endcan

                @can('donation.view')
                <li class="sidebar-item">
                    <a href="{{ route('donation.index') }}" class="sidebar-link">
                        <i class="fas fa-plus-circle"></i>
                        <span class="hide-menu"> Donasi </span>
                    </a>
                </li>
                @endcan
                @can('report.view')
                <li class="sidebar-item">
                    <a href="{{ route('report.index') }}" class="sidebar-link">
                        <i class="fas fa-plus-circle"></i>
                        <span class="hide-menu"> Report </span>
                    </a>
                </li>
                @endcan
                @can('quote-reminder.view')
                <li class="sidebar-item">
                    <a href="{{ route('quote-reminder.index') }}" class="sidebar-link">
                        <i class="fas fa-plus-circle"></i>
                        <span class="hide-menu"> Quote Reminder </span>
                    </a>
                </li>
                @endcan
            </ul>
        </nav>
    </div>
</aside>
