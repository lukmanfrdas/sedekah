@extends('layouts.app')
@section('content')
    <div class="container-fluname">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">{{ ucfirst($module) }}</h4>
                                <h6 class="card-subtitle">Manage {{ $module }}</h6>
                            </div>
                            @can('donation.create')
                                <div class="col-md-4" align="right">
                                    <a href="{{ route($module . '.create') }}" class="btn btn-success btn-lg"><i
                                            class="fa fa-plus"></i> Tambah</a>

                                    <a href="{{ route('donations.export') }}" class="btn btn-warning btn-lg"><i
                                        class="fas fa-share-square"></i> Export</a>
                                </div>
                            @endcan
                        </div>
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>Collector</th>
                                        <th>Donor</th>
                                        <th>Category Donation</th>
                                        <th>Nominal</th>
                                        <th>Tanggal</th>
                                        <th>Status</th>

                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script>
        $(document).ready(function() {
            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route($module . '.index') }}',
                columns: [{
                        data: 'collector_name',
                        name: 'collector_name'
                    },
                    {
                        data: 'donor_name',
                        name: 'donor_name'
                    },
                    {
                        data: 'category_donation_name',
                        name: 'category_donation_name',
                    },
                    {
                        data: 'nominal',
                        name: 'nominal',
                    },
                    {
                        data: 'date',
                        name: 'date',
                    },
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        searchable: false
                    }
                ]
            });
        });
    </script>
@endpush
