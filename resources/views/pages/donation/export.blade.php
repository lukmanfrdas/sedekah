<table>
    <thead>
    <tr>
        <th style="width: 220px; font-weight:bold" >Collector</th>
        <th style="width: 100px; font-weight:bold" >Donor</th>
        <th style="width: 100px; font-weight:bold" >Kategori Donasi</th>
        <th style="width: 450px; font-weight:bold" >Nominal</th>
        <th colspan="2" style="width: 450px; font-weight:bold" >Date</th>
    </tr>
    </thead>
    <tbody>
    @foreach($donations as $data)
        <tr>
            <td>{{ $data->collector->name }}</td>
            <td>{{ $data->donor->name }}</td>
            <td>{{ $data->category_donation->name }}</td>
            <td>{{ $data->nominal }}</td>
            <td>{{ $data->date }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
