@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="card-title">Rumah Sakit</h4>
                            <h6 class="card-subtitle">Detail Rumah Sakit</h6>
                        </div>
                        <div class="col-md-4" align="right">
                            <a href="{{ route($module . '.index') }}" class="btn btn-danger btn-lg"><i class="m-r-10 mdi mdi-backspace"></i>Kembali</a>
                        </div>
                    </div>
                    <hr>
                    <div class="form form-horizontal">
                        @foreach ($shows as $key => $item)
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">{{ ucfirst($item) }}</label>
                            <div class="col-sm-10">
                                {{ $detail->{$key} }}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="col-md-6">
                            <h4 class="card-title">Kuota</h4>
                            <h6 class="card-subtitle">Pengelolaan Kuota Rumah Sakit</h6>
                        </div>
                        <div class="d-flex">
                            @can('quota.create')
                            <div class="mr-2" align="right">
                                <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#quotaModal"><i class="fa fa-plus"></i> Tambah</button>
                            </div>
                            @endcan
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Stase</th>
                                    <th>Kuota</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="quotaModal" tabindex="-1" role="dialog" aria-labelledby="quotaModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Atur Kuota</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! form($form) !!}
                </div>
            </div>
        </div>
    </div>


</div>
@stop

@push('js')
<script>
    $(document).ready(function() {
        $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: "GET",
                url: '{{ route("quota.index") }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    hospital_id: '{{ $detail->id }}'
                }
            },
            columns: [{
                    data: 'stase',
                    name: 'stase'
                },
                {
                    data: 'quota',
                    name: 'quota'
                },
                {
                    data: 'action',
                    name: 'action',
                    "searchable": false
                }
            ]
        });

        let isEdit = false
        let current_quota_id = 0
        $('#btn-add-quota').click(function() {
            const body = {
                hospital_id: '{{ $detail->id }}',
                stase_id: $('#stase_id').val(),
                quota: $('#quota').val()
            }
            
            if (!isEdit) {
                $.post('{{ route("quota.store") }}', body).done(function(data) {
                    $('#quotaModal').modal('hide')
                    $('.data-table').DataTable().ajax.reload()
                })
            } else {
                const url = '{{ url("quota/") }}/' + current_quota_id
                $.ajax({
                    url: url,
                    type: 'PUT',
                    data: body,
                    success: function(res) {
                        $('#quotaModal').modal('hide')
                        $('.data-table').DataTable().ajax.reload()
                    }
                })
            }
        })

        $('body').on('click', '.btn-edit', function() {
            isEdit = true
            const data = $(this).data('model')
            current_quota_id = data.id

            $('#quota').val(data.quota)
            $('#stase_id').val(data.stase_id).trigger('change')
        })

        $('#quotaModal').on('hidden.bs.modal', function () { // when modal is closed
            isEdit = false
            $('#quota').val(null)
            $('#stase_id').val(null).trigger('change')
        });
    });

</script>
@endpush