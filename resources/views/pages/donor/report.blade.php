@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">{{ ucfirst($module) }}</h4>
                                <h6 class="card-subtitle">Manage {{ $module }}</h6>
                            </div>
                        </div>
                        <hr>

                        <form action="{{ route($module . '.index') }}" method="get">
                            <div class="row">
                                <div class="col-md-6">
                                    <select name="donor_id" class="form-control select2" >
                                        <option disabled selected>Select Donor</option>
                                        @foreach ($donors as $donor)
                                            <option value="{{ $donor->id }}">{{ $donor->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary mt-1" type="button">
                                        <small>Search</small>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <hr>

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>Donor</th>
                                        @for ($i=1;$i<=31;$i++)
                                        <th>{{ $i }}</th>
                                        @endfor
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @foreach ($reports as $report)
                                    <tr>

                                        <td>{{ $report['donor']->name }}</td>
                                        @foreach ($report['donation'] as $donation)
                                        <td>{{ $donation }}</td>
                                        @endforeach
                                    </tr>
                                    @endforeach --}}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
