@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title">{{ ucfirst($module) }}</h4>
                                <h6 class="card-subtitle">Manage {{ $module }}</h6>
                            </div>
                        </div>
                        <hr>

                        <form action="{{ route($module . '.index') }}" method="get">
                            <div class="row">
                                <div class="col-md-5">
                                        <div class="form-group">
                                        <select name="collector_id" class="form-control select2" >
                                            <option disabled selected>Select Collector</option>
                                            @foreach ($list_collectors as $collector)
                                                <option value="{{ $collector->id }}" {{ $collector_id == $collector->id ? 'selected' : '' }}>{{ $collector->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                        <div class="form-group">
                                        <select name="donor_id" class="form-control select2" >
                                            <option disabled selected>Select Donor</option>
                                            @foreach ($list_donors as $donor)
                                                <option value="{{ $donor->id }}" {{ $donor_id == $donor->id ? 'selected' : '' }}>{{ $donor->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" type="button">
                                        Search
                                    </button>
                                </div>
                            </div>
                        </form>
                        <hr>

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>Collector</th>
                                        <th>Donor</th>
                                        @for ($i=1;$i<=12;$i++)
                                        <th>{{ $i }}</th>
                                        @endfor
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($reports) > 0)
                                        @foreach ($reports as $report)
                                        <tr>
                                            <td>{{ @$report['donor']->collector->name }}</td>
                                            <td>{{ $report['donor']->name }}</td>
                                            @foreach ($report['donation'] as $donation)
                                            <td>{{ number_format($donation) }}</td>
                                            @endforeach
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="14" align="center">
                                                DATA TIDAK ADA
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
