@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <div class='input-group mb-3'>
                    <div id="reportrange"
                        style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="m-r-10">
                            <span class="btn btn-circle btn-lg bg-danger">
                                <i class="ti-clipboard text-white"></i>
                            </span>
                        </div>
                        <div>
                            Pesan Terkirim
                        </div>
                        <div class="ml-auto">
                            <h2 class="m-b-0 font-light">{{ $sent }}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="m-r-10">
                            <span class="btn btn-circle btn-lg btn-info">
                                <i class="ti-wallet text-white"></i>
                            </span>
                        </div>
                        <div>
                            Pesan Gagal
                        </div>
                        <div class="ml-auto">
                            <h2 class="m-b-0 font-light">{{ $fail }}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="m-r-10">
                            <span class="btn btn-circle btn-lg bg-success">
                                <i class="ti-shopping-cart text-white"></i>
                            </span>
                        </div>
                        <div>
                            Jumlah No HP
                        </div>
                        <div class="ml-auto">
                            <h2 class="m-b-0 font-light">{{ $phone }}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="m-r-10">
                            <span class="btn btn-circle btn-lg bg-warning">
                                <i class="mdi mdi-currency-usd text-white"></i>
                            </span>
                        </div>
                        <div>
                            Total
                        </div>
                        <div class="ml-auto">
                            <h2 class="m-b-0 font-light">{{ $total }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card o-income">
            <div class="card-body">
                <div class="d-flex m-b-30 no-block">
                    <h5 class="card-title m-b-0 align-self-center chart-type">
                        <a data-value="sent" class="btn btn-primary btn-report" href="#" role="button">Pesan Terkirim</a>
                        <a data-value="fail" class="btn btn-primary btn-report" href="#" role="button">Pesan Gagal</a>
                        <a data-value="total" class="btn btn-primary btn-report" href="#" role="button">Total</a>
                    </h5>

                    <div class="ml-auto" id="color-info"></div>
                </div>
                <div id="line-report"></div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link href="{{ asset('assets/libs/morris.js/morris.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
@endpush
@push('js')
    <script src="{{ asset('assets/libs/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/libs/morris.js/morris.min.js') }}"></script>
    <script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
    <script>
        $(function() {
            var start = moment().subtract(1, 'months');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                opens: 'left',
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

            $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
                //do something, like clearing an input
                $('#reportrange').val('');
            });
            $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
                setBar(picker.startDate.format('YYYY-MM-DD'), picker.endDate.format('YYYY-MM-DD'));
            });
            $(".btn-report").click(function() {
                if ($(this).hasClass('btn-primary')) {
                    $(".btn-report").removeClass('btn-secondary').addClass('btn-primary');
                    $(this).removeClass('btn-primary').addClass('btn-secondary');
                    var picker = $('#reportrange').data('daterangepicker');
                    setBar(picker.startDate.format('YYYY-MM-DD'), picker.endDate.format('YYYY-MM-DD'));
                }
            });
            $(".chart-type a:first").trigger('click');
        });

        function setBar(start, end) {
            $("#line-report").empty();

            var active = $(".btn-report.btn-secondary").data('value');
            var chart = Morris.Line({
                element: 'line-report',
                xkey: 'date',
                ykeys: [active],
                labels: [active],
                xLabelAngle: 60,
                xLabelMargin: 10,
                lineColors: ['#7460ee'],
            });

            $.getJSON("{{ route('home.report') }}", {
                    start,
                    end,
                    type: active,
                },
                function(data, textStatus, jqXHR) {
                    chart.setData(data);
                    $("#color-info").html('<ul class="list-inline text-right">' +
                        '<li class="list-inline-item">' +
                        '<h5><i class="fa fa-circle m-r-5 text-primary"></i>'+active+'</h5>' +
                        '</li>' +
                        '</ul>');
                }
            );

        }
    </script>
@endpush
