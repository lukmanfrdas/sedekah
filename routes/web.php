<?php

use App\Http\Controllers\Auth\GoogleController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryDonationController;
use App\Http\Controllers\CollectorController;
use App\Http\Controllers\DonationController;
use App\Http\Controllers\DonorController;
use App\Http\Controllers\QuoteReminderController;
use App\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('auth/google', [GoogleController::class, 'redirectToGoogle'])->name('auth.google');
Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);

Route::group(['middleware' => ['auth']], function () {
    Route::get('', [HomeController::class, 'index'])->name('home');
    Route::get('home/report', [HomeController::class, 'report'])->name('home.report');
    Route::patch('user/login-as/{id}', [UserController::class, 'loginAs'])->name('user.login-as');
    Route::post('user/topup-credit/{id}', [UserController::class, 'topupCredit'])->name('user.topup-credit');
    Route::post('user/topup-saldo/{id}', [UserController::class, 'topupSaldo'])->name('user.topup-saldo');
    Route::post('user/cut-credit/{id}', [UserController::class, 'cutCredit'])->name('user.cut-credit');
    Route::post('user/cut-saldo/{id}', [UserController::class, 'cutSaldo'])->name('user.cut-saldo');
    Route::resource('user', UserController::class);
    Route::resource('permission', PermissionController::class);
    Route::resource('role', RoleController::class);
    Route::resource('category-donation', CategoryDonationController::class);
    Route::resource('collector', CollectorController::class);
    Route::get('donor/report', [DonorController::class, 'donorReport'])->name('donor.report');
    Route::resource('donor', DonorController::class);

    Route::resource('donation', DonationController::class);
    Route::resource('quote-reminder', QuoteReminderController::class);

    Route::get('report', [ReportController::class, 'index'])->name('report.index');
    Route::get('donations/export', [DonationController::class, 'export'])->name('donations.export');



});

Route::get('check-time', function () {
    return date('Y-m-d H:i:s');
});
