<?php

namespace App\Providers;

use App\Services\BalanceService;
use Illuminate\Support\ServiceProvider;

class SaldoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // view()->composer(
        //     ['layouts.*'],
        //     function ($view) {
        //         $balance = BalanceService::getBalance();
        //         $view->with([
        //             'balance' => (float) $balance,
        //         ]);
        //     }
        // );
    }
}
