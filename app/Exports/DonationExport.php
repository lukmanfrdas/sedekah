<?php

namespace App\Exports;

use App\Models\CategoryDonation;
use App\Models\Donation;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class DonationExport implements FromView
{
    public function view(): View
    {
        return view('pages.donation.export', [
            'donations' => Donation::all(),
            'category_donations' => CategoryDonation::all()
        ]);
    }
}
