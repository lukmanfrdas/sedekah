<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donation extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'user_id',
        'collector_id',
        'donor_id',
        'category_donation_id',
        'nominal',
        'date',
        'status'
    ];

    public function collector()
    {
        return $this->belongsTo(User::class, 'collector_id');
    }

    public function donor()
    {
        return $this->belongsTo(User::class, 'donor_id')->withTrashed();
    }

    public function category_donation()
    {
        return $this->belongsTo(CategoryDonation::class)->withTrashed();
    }

}
