<?php

namespace App\Forms;

use App\Models\CategoryDonation;
use App\Models\Donor;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\Form;

class DonationForm extends Form
{
    public function __construct()
    {
        $this->user = Auth::user();
    }
    public function buildForm()
    {
        if (in_array($this->user->type, ['admin'])) {
            $this
            ->add('collector_id', 'choice', [
                'choices' => $this->getCollectors(),
                'empty_value' => 'Pilih Collector',
                'label' => 'Collector', 'attr' => ['class' => 'form-control select2']
            ]);
        }
        $this
        ->add('donor_id', 'choice', [
            'choices' => $this->getDonors(),
            'empty_value' => 'Pilih Donor',
            'label' => 'Donor *', 'attr' => ['class' => 'form-control select2']
        ])
        ->add('category_donation_id', 'choice', [
            'choices' => $this->getCategoryDonations(),
            'empty_value' => 'Pilih Kategori Donasi',
            'label' => 'Kategori Donasi *', 'attr' => ['class' => 'form-control select2']
        ])
        ->add('nominal', 'text', [
            'label' => trans('Nominal')
        ])
        ->add('date', 'date', [
            'label' => trans('Tanggal')
        ])
        ->add('status', 'choice', [
            'choices' => [
                'approve' => 'approve',
                'pending' => 'pending'
            ],
            'empty_value' => 'Pilih Status',
            'label' => 'Status', 'attr' => ['class' => 'form-control select2']
        ])
        ->add('submit', 'submit', ['label' => 'Submit', 'attr' => ['class' => 'btn btn-success']]);
    }

    public function getUsers()
    {
        $users = User::whereIn('type', ['collector', 'user'])->orderBy('name', 'ASC')->get();

        $data = [];
        foreach ($users as $user) $data[$user->id] = $user->name;

        return $data;
    }

    public function getCollectors()
    {
        $users = User::whereType('collector')->get();
        $data = [];
        foreach ($users as $user) $data[$user->id] = $user->name;
        return $data;
    }

    public function getDonors()
    {
        $users = User::get();
        $data = [];
        foreach ($users as $user) $data[$user->id] = $user->name;
        return $data;
    }

    public function getCategoryDonations()
    {
        $category_donations = CategoryDonation::get();
        $data = [];
        foreach ($category_donations as $category_donation) {
            $data[$category_donation->id] = $category_donation->name;
        }
        return $data;
    }
}

