<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class QuoteReminderForm extends Form
{
    public function buildForm()
    {
        $this
        ->add('description', 'textarea', [
            'label' => trans('Deskripsi')
        ])
        ->add('submit', 'submit', ['label' => 'Simpan', 'attr' => ['class' => 'btn btn-success']]);
    }
}
