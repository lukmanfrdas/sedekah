<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CategoryDonationForm extends Form
{
    public function buildForm()
    {
        $this
        ->add('name', 'text', [
            'label' => trans('Kategori Donasi')
        ])
        ->add('submit', 'submit', ['label' => 'Simpan', 'attr' => ['class' => 'btn btn-success']]);
    }
}
