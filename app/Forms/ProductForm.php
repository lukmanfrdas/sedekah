<?php

namespace App\Forms;

use App\Models\Category;
use Kris\LaravelFormBuilder\Form;

class ProductForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('category_id', 'choice', [
                'choices' => $this->getCategories(),
                'empty_value' => 'Pilih Category',
                'label' => 'Category *', 'attr' => ['class' => 'form-control select2']
            ])
            ->add('name', 'text')
            ->add('price', 'number')
            ->add('margin_agent', 'text')
            ->add('image', 'text')
            ->add('sku', 'text')
            ->add('description', 'text')
            ->add('status', 'choice', [
                'choices' => [
                    1 => 'Aktif',
                    9 => 'Tidak Aktif'
                ],
                'empty_value' => 'Pilih Status',
                'label' => 'Status', 'attr' => ['class' => 'form-control select2']
            ])
            ->add('submit', 'submit', ['label'=>'Submit', 'attr'=>['class'=>'btn btn-success']]);
    }

    public function getCategories()
    {
        $categories = Category::get();
        $data = [];
        foreach ($categories as $category) {
            $data[$category->id] = $category->name;
        }
        return $data;
    }
}