<?php

namespace App\Forms;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\Form;

class DonorForm extends Form
{
    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function buildForm()
    {
        if (in_array($this->user->type, ['admin'])) {
            $this
            ->add('collector_id', 'choice', [
                'choices' => $this->getCollectors(),
                'empty_value' => 'Pilih Collector',
                'label' => 'Collector', 'attr' => ['class' => 'form-control select2']
            ]);
        }

        $this
        ->add('name', 'text', [
            'label' => trans('Nama')
        ])
        ->add('email', 'text', [
            'label' => trans('Email')
        ])
        ->add('phone', 'text', [
            'label' => trans('No. Handphone')
        ])
        ->add('address', 'text', [
            'label' => trans('Alamat')
        ])
        ->add('date', 'choice', [
            'choices' => [
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '13' => '13',
                '14' => '14',
                '15' => '15',
                '16' => '16',
                '17' => '17',
                '18' => '18',
                '19' => '19',
                '20' => '20',
                '21' => '21',
                '22' => '22',
                '23' => '23',
                '24' => '24',
                '25' => '25',
                '26' => '26',
                '27' => '27',
                '28' => '28',
                '29' => '29',
                '30' => '30',
                '31' => '31',
            ],
            // 'empty_value' => 'Pilih Tanggal',
            'label' => 'Pilih Tanggal', 'attr' => ['class' => 'form-control select2']
        ])
        // ->add('tanggal', 'number', ['attr' => ['class' => 'form-control']])
        ->add('password', 'password', ['value' => false, 'attr' => ['class' => 'form-control']])
        ->add('submit', 'submit', ['label' => 'Submit', 'attr' => ['class' => 'btn btn-success']]);
    }

    public function getCollectors()
    {
        $users = User::whereType('collector')->get();
        $data = [];
        foreach ($users as $user) $data[$user->id] = $user->name;
        return $data;
    }
}
