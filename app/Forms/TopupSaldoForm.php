<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class TopupSaldoForm extends Form
{
    public function buildForm()
    {
        $this
        ->add('amount', 'text', ['label'=>'Besaran'])
        ->add('notes', 'textarea', ['label'=>'Catatan']);
    }
}
