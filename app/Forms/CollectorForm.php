<?php

namespace App\Forms;

use App\Models\User;
use Kris\LaravelFormBuilder\Form;

class CollectorForm extends Form
{
    public function buildForm()
    {
        $this
        ->add('collector_id', 'choice', [
            'choices' => $this->getCollectors(),
            'empty_value' => 'Pilih Collector',
            'label' => 'Collector', 'attr' => ['class' => 'form-control select2']
        ])

        ->add('name', 'text', ['attr' => ['class' => 'form-control']])
        ->add('email', 'text', ['attr' => ['class' => 'form-control']])
        ->add('phone', 'text', ['attr' => ['class' => 'form-control']])
        ->add('address', 'text', ['attr' => ['class' => 'form-control']])
        ->add('password', 'password', ['value' => false, 'attr' => ['class' => 'form-control']])
        ->add('submit', 'submit', ['label' => 'Submit', 'attr' => ['class' => 'btn btn-success']]);
    }

    public function getCollectors()
    {
        $users = User::whereType('collector')->get();
        $data = [];
        foreach ($users as $user) $data[$user->id] = $user->name;
        return $data;
    }

}
