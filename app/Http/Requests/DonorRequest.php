<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DonorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('donor');
        $unique_email = (!empty($id)) ? 'unique:donors,email,' . $id : 'unique:donors,email';
        $unique_phone = (!empty($id)) ? 'unique:donors,phone,' . $id : 'unique:donors,phone';

        return [
            'name' => 'required',
            'email' => 'required|email|' . $unique_email,
            'phone' => 'required|numeric|' . $unique_phone,
        ];
    }
}
