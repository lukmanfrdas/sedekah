<?php

namespace App\Http\Controllers;

use App\Traits\XIForm;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use App\Http\Requests\DonationRequest;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\User;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\DB;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;

class ReportController extends Controller
{
    private $module, $model, $form;
    protected $repository;
    use XIForm;

    public function __construct(Donation $repository, FormBuilder $formBuilder)
    {
        $this->module = 'report';
        $this->repository = $repository;

        View::share('module', $this->module);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $collector_id   = $request->collector_id;
        $donor_id       = $request->donor_id;
        $list_collectors= User::whereType('collector')->get();
        $list_donors    = User::all();
        $donors         = new User;
        if(!empty($collector_id))
            $donors = $donors->whereCollectorId($collector_id);
        if(!empty($donor_id))
            $donors = $donors->whereId($donor_id);

        $donors = $donors->with('collector')->get();

        $data['reports'] = [];
        foreach($donors as $k=>$donor){
            $data['reports'][$k]['donor'] = $donor;
            for($i = 1; $i <= 12; $i++)
                $data['reports'][$k]['donation'][$i] = Donation::whereDonorId($donor->id)->whereMonth('date',$i)->sum('nominal');
        }
        return view('pages.' . $this->module . '.index', compact('donors', 'list_collectors', 'list_donors', 'collector_id', 'donor_id'), $data);
    }
}
