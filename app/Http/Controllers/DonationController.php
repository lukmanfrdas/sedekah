<?php

namespace App\Http\Controllers;

use App\Exports\DonationExport;
use App\Traits\XIForm;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use App\Http\Requests\DonationRequest;
use App\Models\Donation;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;

class DonationController extends Controller
{
    private $module, $model, $form;
    protected $repository;
    use XIForm;

    public function __construct(Donation $repository, FormBuilder $formBuilder)
    {
        $this->module = 'donation';
        $this->repository = $repository;
        $this->formBuilder = $formBuilder;
        $this->form = 'App\Forms\DonationForm';
        $this->formRequest = 'App\Http\Requests\DonationRequest';

        View::share('module', $this->module);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
{
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        if ($request->ajax()) {
            $data = $this->repository->orderBy('created_at', 'DESC');
            return DataTables::of($data)
            ->addColumn('action', function ($data) {
                $buttons[] = ['type' => 'detail', 'route' => route($this->module . '.show', $data->id), 'label' => 'Detail', 'action' => 'primary', 'icon' => 'share'];
                $buttons[] = ['type' => 'edit', 'route' => route($this->module . '.edit', $data->id), 'label' => 'Edit', 'icon' => 'edit'];
                $buttons[] = ['type' => 'delete', 'label' => 'Delete', 'confirm' => 'Are you sure?', 'route' => route($this->module . '.destroy', $data->id)];

                return $this->icon_button($buttons, true);
            })

            ->addColumn('collector_name', function ($data) {
                return data_get($data, 'collector.name');
            })
            ->addColumn('category_donation_name', function ($data) {
                return data_get($data, 'category_donation.name');
            })
            ->addColumn('donor_name', function ($data) {
                return data_get($data, 'donor.name');
            })
            ->rawColumns(['action', 'role'])
            ->make();
        }
        return view('pages.' . $this->module . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'POST',
            'url' => route($this->module . '.store')
        ]);
        $data['validator'] = JsValidatorFacade::formRequest($this->formRequest);

        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonationRequest $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        DB::beginTransaction();
        try {
            $user = Auth::user();
            $data = $request->all();

            switch ($user->type) {
                case 'admin':
                    $data['collector_id'] = $request->collector_id;
                    $data['donor_id'] = $request->donor_id;
                    $data['user_id'] = $user->id;
                    break;
                case 'collector':
                    $data['collector_id'] = $user->id;
                    $data['donor_id'] = $request->donor_id;
                    $data['user_id'] = $user->id;
                    break;
                default:
                    $data['collector_id'] = $user->collector_id;
                    $data['donor_id'] = $user->id;
                    $data['user_id'] = $user->id;
                break;
            }

            // dd($data);
            $post = $this->repository->create($data);
            gilog("Create " . $this->module, $post, $data);
            DB::commit();
            if ($data['status'] == 'approve') {
                $caption = 'Terima kasih Anda telah ber'. strtolower(data_get($post ,'category_donation.name')) .' sebesar Rp. '.number_format($post->nominal);
                send_whatsapp(data_get($post ,'donor.phone'), $caption);
            }
            flash('Success create ' . $this->module)->success();
        } catch (\Exception $ex) {
            DB::rollBack();
            flash($ex->getMessage())->error();
        }

        return redirect()->route($this->module . '.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();

        $get = $this->repository->find($id);
        $data['detail'] = $get;
        $detail = new Donation();
        $data['shows'] = $detail->getFillable();

        return view('pages.' . $this->module . '.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        $get = $this->repository->find($id);
        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'PUT',
            'url' => route($this->module . '.update', $id),
            'model' => $get
        ]);

        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DonationRequest $request, $id)
    {
        // if (!$request->user()->can($this->module . '.create')) return notPermited();
        if (!$request->user()->can($this->module . '.update')) return notPermited();


        DB::beginTransaction();
        try {
            $user = Auth::user();
            $data = $request->all();

            switch ($user->type) {
                case 'admin':
                    $data['collector_id'] = $request->collector_id;
                    $data['donor_id'] = $request->donor_id;
                    $data['user_id'] = $user->id;
                    break;
                case 'collector':
                    $data['collector_id'] = $user->id;
                    $data['donor_id'] = $request->donor_id;
                    $data['user_id'] = $user->id;
                    break;
                default:
                    $data['collector_id'] = $user->collector_id;
                    $data['donor_id'] = $user->id;
                    $data['user_id'] = $user->id;
                break;
            }

            // dd($data);
            $post = $this->repository->find($id);
            $post->update($data);
            gilog("Create " . $this->module, $post, $data);
            DB::commit();
            if ($data['status'] == 'approve') {
                $caption = 'Terima kasih Anda telah ber'. strtolower(data_get($post ,'category_donation.name')) .' sebesar Rp. '.number_format($post->nominal);
                send_whatsapp(data_get($post ,'donor.phone'), $caption);
            }
            flash('Success update ' . $this->module)->success();
        } catch (\Exception $ex) {
            DB::rollBack();
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.delete')) return notPermited('json');

        try {
            DB::transaction(function () use ($id) {
                $get = $this->repository->find($id);
                $get->delete($id);
                gilog("Delete " . $this->module, $get, ['notes' => @request('notes')]);
            });
            $data['message'] = 'Success delete ' . $this->module;
            $status = 200;
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }
        return response()->json($data, $status);
    }

    public function export()
    {
        return Excel::download(new DonationExport, 'export-donation.xlsx');
    }
}
