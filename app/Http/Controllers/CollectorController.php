<?php

namespace App\Http\Controllers;

use App\Traits\XIForm;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use App\Http\Requests\CollectorRequest;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Proengsoft\JsValidation\Facades\JsValidatorFacade as JsValidator;
use Illuminate\Support\Str;

class CollectorController extends Controller
{
    private $module, $model, $form;
    protected $repository;
    use XIForm;

    public function __construct(User $repository, FormBuilder $formBuilder)
    {
        $this->module = 'collector';
        $this->repository = $repository;
        $this->formBuilder = $formBuilder;
        $this->formRequest = 'App\Http\Requests\CollectorRequest';
        $this->form = 'App\Forms\CollectorForm';

        View::share('module', $this->module);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->user()->can($this->module . '.view')) return notPermited();


        if ($request->ajax()) {
            $data = $this->repository->where('type', 'collector');
            return DataTables::of($data)
                ->addColumn('action', function ($data) use ($request) {
                    $buttons[] = ['type' => 'detail', 'route' => route($this->module . '.show', $data->id), 'label' => 'Detail', 'action' => 'primary', 'icon' => 'share'];
                    $buttons[] = ['type' => 'edit', 'route' => route($this->module . '.edit', $data->id), 'label' => 'Edit', 'icon' => 'edit'];
                    $buttons[] = ['type' => 'delete', 'label' => 'Delete', 'confirm' => 'Are you sure?', 'route' => route($this->module . '.destroy', $data->id)];

                    if ($request->user()->type == 'super-admin') {
                        $buttons[] = [
                            'label' => 'Login sebagai ' . $data->name,
                            'route' => route($this->module . '.login-as', $data->id),
                            'confirm' => 'Are you sure?',
                            'form' => [],
                            'method' => 'PATCH',
                            'action' => 'info'
                        ];
                    }
                    return $this->icon_button($buttons, true);
                })

                ->addColumn('role', function ($data) {
                    return '<label class="label label-info">' . $data->roles()->pluck('name')->implode('</label> <label class="label label-info">') . '</label>';
                })
                ->addColumn('user_name', function ($data) {
                    return data_get($data, 'user.name');
                })
                ->addColumn('collector_name', function ($data) {
                    return data_get($data, 'collector.name');
                })
                ->addColumn('collector_name', function ($data) {
                    return data_get($data, 'collector.name');
                })
                ->rawColumns(['action', 'role'])

                ->make();
        }
        return view('pages.' . $this->module . '.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'POST',
            'url' => route($this->module . '.store')
        ]);
        $data['validator'] = JsValidator::formRequest($this->formRequest);
        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CollectorRequest $request)
    {
        if (!$request->user()->can($this->module . '.create')) return notPermited();

        try {
            DB::transaction(function () use ($request) {
                $input = $request->all();

                $input['type'] = 2;
                $input['password'] = Hash::make($input['password']);
                $input['type'] = Str::lower(data_get(config('status.type'), $input['type']));

                $post = $this->repository->create($input);

                $this->assignRole($post, $input['type']);
                gilog("Create " . $this->module, $post, $input);
            });
            flash('Success create ' . $this->module)->success();
        } catch (\Exception $ex) {
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show(Request $request, $id)
    // {
    //     if (!$request->user()->can($this->module . '.view')) return notPermited();

    //     $get = $this->repository->find($id);
    //     $data['detail'] = $get;
    //     $detail = new User();
    //     $data['shows'] = $detail->getFillable();

    //     if ($request->ajax()) {
    //         if (data_get($request, 'type') == 'referral') {
    //             $data = User::whereCollectorId($id);

    //             return DataTables::of($data)
    //                 ->addColumn('deposit_amount', function ($data) {
    //                     return number_format($data->deposit_amount);
    //                 })
    //                 ->addColumn('created_at', function ($data) {
    //                     return date('Y-m-d H:i:s', strtotime($data->created_at));
    //                 })
    //                 ->rawColumns(['deposit_amount'])
    //                 ->make();
    //         }

    //         if (data_get($request, 'type') == 'booking') {
    //             $data = Booking::with(['user', 'product'])->where(['bookings.agent_id' => $id]);

    //             return DataTables::of($data)
    //                 ->addColumn('day', function ($data) {
    //                     return 'Day ' . $data->day;
    //                 })
    //                 ->addColumn('status', function ($data) {
    //                     return data_get(config('status.active'), $data->status);
    //                 })
    //                 ->addColumn('type', function ($data) {
    //                     return data_get(config('status.booking_type'), $data->type);
    //                 })
    //                 ->addColumn('created_at', function ($data) {
    //                     return date('Y-m-d H:i:s', strtotime($data->created_at));
    //                 })
    //                 ->rawColumns(['day'])
    //                 ->make();
    //         }
    //     }

    //     return view('pages.' . $this->module . '.show', $data);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        $get = $this->repository->find($id);
        $data['form'] = $this->formBuilder->create($this->form, [
            'method' => 'PUT',
            'url' => route($this->module . '.update', $id),
            'model' => $get
        ]);

        return view('pages.' . $this->module . '.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.update')) return notPermited();

        try {
            DB::transaction(function () use ($id, $request) {
                $input = $request->all();
                if ($input['password']) {
                    $input['password'] = Hash::make($input['password']);
                } else {
                    unset($input['password']);
                }

                $input['type'] = 2;
                $input['type'] = Str::lower(data_get(config('status.type'), $input['type']));

                $post = $this->repository->find($id);
                $post->update($input);
                $this->assignRole($post, $input['type']);

                gilog("Create " . $this->module, $post, $input);
            });
            flash('Success update ' . $this->module)->success();
        } catch (\Exception $ex) {
            flash($ex->getMessage())->error();
        }
        return redirect()->route($this->module . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->user()->can($this->module . '.delete')) return notPermited('json');

        try {
            DB::transaction(function () use ($id) {
                $get = $this->repository->find($id);
                $get->delete($id);
                gilog("Delete " . $this->module, $get, ['notes' => @request('notes')]);
            });
            $data['message'] = 'Success delete ' . $this->module;
            $status = 200;
        } catch (\Exception $ex) {
            $data['message'] = $ex->getMessage();
            $status = 500;
        }
        return response()->json($data, $status);
    }

    public function assignRole($model, $roles = '')
    {
        foreach ($model->roles as $key => $value) {
            $model->removeRole($value);
        }
        if (isset($roles)) {
            $model->assignRole($roles);
        }
    }

    public function loginAs(Request $request, $id)
    {
        if (Auth::user()->type == 0) {
            $request->session()->put('admin_login', Auth::user()->id);
        } else {
            $request->session()->forget('admin_login');
        }
        Auth::loginUsingId($id);
        $data['status'] = 1;
        $data['redirect'] = url('/');
        return response()->json($data, 200);
    }
}
