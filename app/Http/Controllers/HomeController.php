<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [];
        $data['sent'] = 100;
        $data['fail'] = 100;
        $data['phone'] = 100;
        $data['total'] = number_format(1000000);
        return view('home', $data);
    }

    public function report(Request $request)
    {
        $data = [];
        return response()->json($data, 200);
    }
}
