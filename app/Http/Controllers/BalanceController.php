<?php

namespace App\Http\Controllers;

use App\Traits\XIForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use Yajra\DataTables\Facades\DataTables;
use App\Exports\BalanceExport;
use App\Models\Balance;
use Maatwebsite\Excel\Facades\Excel;

class BalanceController extends Controller
{
    use XIForm;
    public function __construct(Balance $repository, FormBuilder $formBuilder)
    {
        $this->repository = $repository;
        $this->formBuilder = $formBuilder;
        $this->module = 'balance';
        $this->permissions = ["view", "create", "update", "delete"];
        View::share(['module' => $this->module]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repository
                ->orderBy('created_at', 'desc')
                ->where(['category' => 'SALDO']);

            if (!$request->user()->can('balance.view.all')) {
                $data = $data->where('user_id', $request->user()->id);
            }

            return DataTables::of($data)
                ->addColumn('type', function ($data) {
                    return '<span class="badge badge-' . ($data->type == 'debit' ? 'danger' : 'success') . '">' . $data->type . '</span>';
                })
                ->addColumn('status', function ($data) {
                    return '<span class="badge badge-' . ($data->status == 0 ? 'secondary' : (($data->status == 99 || $data->status == 999) ? 'danger' : 'success')) . '">' . config('status.balance')[$data->status] . '</span>';
                })
                ->addColumn('amount', function ($data) {
                    return '<span class="text-' . ($data->type == 'debit' ? 'danger' : 'success') . '">' . ($data->type == 'debit' ? '-' : '+') . ' Rp.' . number_format($data->amount) . '</span>';
                })
                ->addColumn('saldo', function ($data) {
                    return 'Rp.' . number_format($data->saldo);
                })
                ->addColumn('notes', function ($data) use ($request) {
                    $return = $data->notes;
                    if ($request->user()->type == 'super-admin') {
                        $return .= '<br><small><a href="' . route($this->module . '.index', ['user_id' => $data->user_id]) . '">' . $data->user->email . '</a></small>';
                    }
                    return $return;
                })
                ->filterColumn('notes', function ($query, $keyword) {
                    $sql = "notes LIKE ?";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                })
                ->rawColumns(['type', 'status', 'amount', 'notes'])
                ->make();
        }
        return view('pages.' . $this->module . '.index', ['url' => route($this->module . '.index', ['user_id' => $request->user_id])]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export(Request $request)
    {
        return Excel::download(new BalanceExport($request->user()->id, $request->status), $request->status . '-' . date('Ymd') . '.xlsx');
    }
}
