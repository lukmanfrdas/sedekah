<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class QueryAddPermissionDonorToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = [
            'donor.view',
            'donor.create',
            'donor.update',
            'donor.delete',
        ];
        foreach ($permissions as $permission) Permission::updateOrCreate(['name' => $permission]);

        $role_admin = Role::whereName('admin')->first();
        $role_admin->givePermissionTo($permissions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permissions = [
            'donor.view',
            'donor.create',
            'donor.update',
            'donor.delete',
        ];
        foreach ($permissions as $permission) Permission::updateOrCreate(['name' => $permission]);

        $role_admin = Role::whereName('admin')->first();
        $role_admin->revokePermissionTo($permissions);
    }
}
